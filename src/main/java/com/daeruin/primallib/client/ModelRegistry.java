package com.daeruin.primallib.client;

import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ModelRegistry
{
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event)
    {
        PrimalUtilReg.registerBlockModels(PrimalBlockRegistry.getBlocks());
        PrimalUtilReg.registerItemModels(PrimalItemRegistry.getItems());
    }
}
