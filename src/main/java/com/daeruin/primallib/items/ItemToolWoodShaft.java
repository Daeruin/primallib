package com.daeruin.primallib.items;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.Loader;

import javax.annotation.Nonnull;

public class ItemToolWoodShaft extends ItemToolBasic
{
    ItemToolWoodShaft(String registryName, ToolMaterial material, float attackDamage, float attackSpeed)
    {
        super(registryName, material, attackDamage, attackSpeed);
        this.setMaxDamage(18);
        this.efficiency = 2.0F;
        this.setHarvestLevel("shovel", 0);
    }

    @Override
    public void onUsingTick(ItemStack stack, EntityLivingBase player, int tick)
    {
        if (Loader.isModLoaded("darkestbeforedawn")
                && Item.getByNameOrId("darkestbeforedawn:glowing_ember") != null
                && player instanceof EntityPlayer)
        {
            World world = player.world;
            RayTraceResult ray = this.rayTrace(player.world, (EntityPlayer) player, false);
            BlockPos pos = ray.getBlockPos();

            if (world.isRemote && (tick == 30 || tick == 10 || tick == 1))
            {
                double xCoord = (double) pos.getX() + 0.5D;
                double yCoord = (double) pos.getY() + 0.2D;
                double zCoord = (double) pos.getZ() + 0.5D;
                world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, xCoord, yCoord, zCoord, 0.0D, 0.0D, 0.0D);
            }

            if (!world.isRemote && !world.isRainingAt(pos.up()) && tick == 1)
            {
                boolean isCreative = ((EntityPlayer) player).isCreative();
                double rand = Math.random();

                if (rand < 0.4 || isCreative)
                {
                    Item emberItem = Item.getByNameOrId("darkestbeforedawn:glowing_ember");

                    if (emberItem != null)
                    {
                        ItemStack emberItemStack = new ItemStack(emberItem, 1);
                        // ItemStack ember = new ItemStack(com.daeruin.darkestbeforedawn.items.ItemRegistry.GLOWING_EMBER, 1);
                        EntityItem entity = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), emberItemStack);
                        world.spawnEntity(entity);
                    }

                }

                if (!isCreative)
                {
                    stack.damageItem(1, player);

                    if (rand > 0.9)
                    {
                        world.setBlockToAir(pos);
                        player.playSound(SoundEvents.BLOCK_LADDER_BREAK, 1.0F, 1.0F);
                    }
                }
            }
        }
    }

    @Override
    @Nonnull
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (Loader.isModLoaded("darkestbeforedawn"))
        {
            Block fireboardBlock = Block.getBlockFromName("darkestbeforedawn:fireboard");

            if (fireboardBlock != null)
            {
                IBlockState fireboardBlockDefaultState = fireboardBlock.getDefaultState();

                if (worldIn.getBlockState(pos) == fireboardBlockDefaultState) // Shaft can only be used on a fireboard
                {
                    playerIn.setActiveHand(hand); // Required for onUsingTick to be called - not sure how or why
                    return EnumActionResult.PASS; // Seems to work with either PASS or SUCCESS
                }
            }
        }

        return EnumActionResult.FAIL;
    }

    @Override
    public int getItemBurnTime(ItemStack itemStack)
    {
        return 100; // Wooden shaft burns for 100 ticks.
    }

    @Override
    public int getMaxItemUseDuration(ItemStack itemStack)
    {
        return 70; // Action lasts for 70 ticks
    }

    @Override
    @Nonnull
    public EnumAction getItemUseAction(ItemStack itemStack)
    {
        return EnumAction.BOW;
    }

    @Override
    public boolean hasContainerItem(ItemStack stack)
    {
        return false;
    }
}
