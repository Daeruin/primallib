package com.daeruin.primallib.recipes;

import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.util.PrimalUtil;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.state.IBlockState;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import javax.annotation.Nonnull;

/*
    The recipe system turns every JSON recipe into an object that implements the IRecipe interface
    The IRecipe object has getters for the recipe's ResourceLocation, ingredients list, and crafting result
    This IRecipe implementation extends ShapelessOreRecipe
    The only difference is its override of getRemainingItems, which keeps axes in the crafting grid and turns logs into stripped logs (also keeping them in the crafting grid)
    The other methods required by the IRecipe interface are already implemented by ShapelessOreRecipe
 */
@SuppressWarnings("unused")
public class PrimalRecipe extends ShapelessOreRecipe implements IRecipe
{
    private final String primalRecipeGroup;

    PrimalRecipe(ParsedPrimalRecipe parsedPrimalRecipe)
    {
        // super(group, input, result);
        super(parsedPrimalRecipe.getResourceLocation(), parsedPrimalRecipe.getIngredientList(), parsedPrimalRecipe.getCraftingResult());

        this.primalRecipeGroup = parsedPrimalRecipe.getRecipeType();
    }

    @Override
    @Nonnull
    public NonNullList<ItemStack> getRemainingItems(InventoryCrafting craftingInventory)
    {
        if (primalRecipeGroup != null)
        {
            NonNullList<ItemStack> remainingItems = NonNullList.withSize(craftingInventory.getSizeInventory(), ItemStack.EMPTY);

            for (int i = 0; i < remainingItems.size(); i++)
            {
                ItemStack stack = craftingInventory.getStackInSlot(i);

                if (!stack.isEmpty())
                {
                    Item item = stack.getItem();

                    // If it's an axe, damage the axe and keep it in the crafting grid
                    if (PrimalUtil.isAxe(item) && (primalRecipeGroup.equals("bark_from_log") || primalRecipeGroup.equals("wooden_shaft")))
                    {
                        // remainingItems.set(i, damageAxe(stack.copy()));
                        remainingItems.set(i, stack.copy());
                    }
                    // If it's a vanilla log, replace it with a stripped log
                    else if (Block.getBlockFromItem(item) instanceof BlockLog && primalRecipeGroup.equals("bark_from_log"))
                    {
                        Block block = Block.getBlockFromItem(item);
                        int meta = stack.getMetadata();
                        @SuppressWarnings("deprecation") IBlockState state = block.getStateFromMeta(meta);
                        Block remainingBlock = PrimalUtil.getStrippedLog(state);

                        remainingItems.set(i, new ItemStack(remainingBlock));
                    }
                    else // Item is not a log - see if it has a container item
                    {
                        remainingItems.set(i, ForgeHooks.getContainerItem(stack));
                    }
                }
            }

            return remainingItems;
        }
        else
        {
            return super.getRemainingItems(craftingInventory);
        }
    }

    @Nonnull
    @Override
    public ItemStack getCraftingResult(@Nonnull InventoryCrafting craftingInventory)
    {
        if (primalRecipeGroup.equals("bark_from_log"))
        {
            this.output.setCount(PrimalConfig.BARK_AND_STRIPPED_LOGS.barkAmount);

            return this.output;
        }
        else if (primalRecipeGroup.equals("planks_from_stripped_logs"))
        {
            this.output.setCount(PrimalConfig.BARK_AND_STRIPPED_LOGS.plankAmount);

            return this.output;
        }

        return super.getCraftingResult(craftingInventory);
    }

    // private ItemStack damageAxe(final ItemStack stack)
    // {
    //     EntityPlayer craftingPlayer = ForgeHooks.getCraftingPlayer();
    //
    //     if (stack.attemptDamageItem(1, new Random(), craftingPlayer instanceof EntityPlayerMP ? (EntityPlayerMP) craftingPlayer : null))
    //     {
    //         ForgeEventFactory.onPlayerDestroyItem(craftingPlayer, stack, null);
    //         return ItemStack.EMPTY;
    //     }
    //
    //     return stack;
    // }
}
