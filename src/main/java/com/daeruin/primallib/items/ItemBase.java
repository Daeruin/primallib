package com.daeruin.primallib.items;

import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemBase extends Item
{
    private int burnTime;

    public ItemBase(String registryName)
    {
        this(registryName, 0);
    }

    public ItemBase(String registryName, int burnTime)
    {
        this.burnTime = burnTime;
        PrimalUtilReg.initializeItem(this, registryName);
    }

    @Override
    public int getItemBurnTime(ItemStack itemStack)
    {
        return burnTime;
    }
}
