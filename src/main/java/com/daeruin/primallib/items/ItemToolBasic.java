package com.daeruin.primallib.items;

import com.daeruin.primallib.util.PrimalUtilReg;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Set;

public class ItemToolBasic extends ItemTool
{
    private static final Set<Block> EFFECTIVE_BLOCKS = Sets.newHashSet(new Block[]{});

    ItemToolBasic(String registryName, ToolMaterial material, float attackDamage, float attackSpeed)
    {
        super(attackDamage, attackSpeed, material, EFFECTIVE_BLOCKS);
        PrimalUtilReg.initializeItem(this, registryName);
    }

    // Needed so tools are damaged when used in crafting grid
    @Override
    public boolean hasContainerItem(ItemStack stack)
    {
        return true;
    }

    @Override
    @Nonnull
    @ParametersAreNonnullByDefault
    public ItemStack getContainerItem(ItemStack itemStack)
    {
        ItemStack result = itemStack.copy();
        result.setItemDamage(itemStack.getItemDamage() + 1);
        return result;
    }
}
