package com.daeruin.primallib.blocks;

import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.LinkedHashSet;

public class PrimalBlockRegistry
{
    public static final Block STRIPPED_OAK_LOG = new PrimalBlockLog("log_stripped_oak");
    public static final Block STRIPPED_SPRUCE_LOG = new PrimalBlockLog("log_stripped_spruce");
    public static final Block STRIPPED_BIRCH_LOG = new PrimalBlockLog("log_stripped_birch");
    public static final Block STRIPPED_JUNGLE_LOG = new PrimalBlockLog("log_stripped_jungle");
    public static final Block STRIPPED_ACACIA_LOG = new PrimalBlockLog("log_stripped_acacia");
    public static final Block STRIPPED_DARK_OAK_LOG = new PrimalBlockLog("log_stripped_dark_oak");

    public static LinkedHashSet<Block> getBlocks()
    {
        LinkedHashSet<Block> BLOCKS = new LinkedHashSet<>();

        BLOCKS.add(STRIPPED_OAK_LOG);
        BLOCKS.add(STRIPPED_SPRUCE_LOG);
        BLOCKS.add(STRIPPED_BIRCH_LOG);
        BLOCKS.add(STRIPPED_JUNGLE_LOG);
        BLOCKS.add(STRIPPED_ACACIA_LOG);
        BLOCKS.add(STRIPPED_DARK_OAK_LOG);

        return BLOCKS;
    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event)
        {
            PrimalUtilReg.registerBlocks(event, getBlocks());
        }

        @SubscribeEvent
        public static void registerItemBlocks(RegistryEvent.Register<Item> event)
        {
            PrimalUtilReg.registerItemBlocks(event, getBlocks());
        }
    }
}
