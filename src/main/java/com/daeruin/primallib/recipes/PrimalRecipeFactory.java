package com.daeruin.primallib.recipes;

import com.daeruin.primallib.util.PrimalUtilRecipe;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.common.crafting.IRecipeFactory;
import net.minecraftforge.common.crafting.JsonContext;

// This factory is referenced in resources > assets > primallib > recipes > _factories.json
// _factories.json assigns this factory to the "primallib_recipe" recipe type
// When a recipe's "type" property is defined as "primallib_recipe", it will use this factory to parse the recipe
// We have to use this factory instead of ShapelessOreRecipeFactory because we need it to return the BarksFromLogRecipe object
// This factory turns the recipe into a PrimalRecipe object
// PrimalRecipe extends ShapelessOreRecipe
// The only difference is its override of getRemainingItems, which keeps the axe in the crafting grid and turns logs into stripped logs (also keeping them in the crafting grid)
public class PrimalRecipeFactory implements IRecipeFactory
{
    public IRecipe parse(final JsonContext context, final JsonObject json)
    {
        // Pass the recipe JSON to my custom parsing method and create a ParsedPrimalRecipe object
        // The ParsedPrimalRecipe will have the recipe's ResourceLocation, ingredients, and crafting result as fields
        ParsedPrimalRecipe parsedPrimalRecipe = PrimalUtilRecipe.parsePrimalRecipe(context, json);

        // Feed the ParsedPrimalRecipe into the constructor of my extension of ShapelessOreRecipe and return it
        return new PrimalRecipe(parsedPrimalRecipe);
    }
}
