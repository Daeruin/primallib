package com.daeruin.primallib.items;

import com.daeruin.primallib.PrimalLib;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.LinkedHashSet;

public final class PrimalItemRegistry
{
    public static final Item PLANT_FIBER = new ItemBase("plant_fiber", 10);
    public static final Item TWINE = new ItemBase("twine", 20);

    // PrimalLib has specific bark types because birch bark is used by Darkest Before Dawn
    public static final Item OAK_BARK = new ItemBase("bark_oak", 40);
    public static final Item SPRUCE_BARK = new ItemBase("bark_spruce", 40);
    public static final Item BIRCH_BARK = new ItemBase("bark_birch", 40);
    public static final Item JUNGLE_BARK = new ItemBase("bark_jungle", 40);
    public static final Item ACACIA_BARK = new ItemBase("bark_acacia", 40);
    public static final Item DARK_OAK_BARK = new ItemBase("bark_dark_oak", 40);

    // Specific bark strips and twigs are in BasketCase
    public static final Item BARK_STRIPS_GENERIC = new ItemBase("bark_strips_generic", 30);
    public static final Item TWIG_GENERIC = new ItemBase("twig_generic", 30);

    public static final Item BRANCH = new ItemBase("branch", 100);
    // Attack damage 1 and speed 1 gives damage per second of 1 - same as wooden hoe
    public static final Item WOODEN_SHAFT = new ItemToolWoodShaft("wooden_shaft", PrimalLib.PRIMAL_WOOD, 1.0F, 1.0F).setNoRepair();

    public static LinkedHashSet<Item> getItems()
    {
        LinkedHashSet<Item> ITEMS = new LinkedHashSet<>();

        ITEMS.add(PLANT_FIBER);
        ITEMS.add(TWINE);
        ITEMS.add(OAK_BARK);
        ITEMS.add(SPRUCE_BARK);
        ITEMS.add(BIRCH_BARK);
        ITEMS.add(JUNGLE_BARK);
        ITEMS.add(ACACIA_BARK);
        ITEMS.add(DARK_OAK_BARK);
        ITEMS.add(BARK_STRIPS_GENERIC);
        ITEMS.add(TWIG_GENERIC);
        ITEMS.add(BRANCH);
        ITEMS.add(WOODEN_SHAFT);

        return ITEMS;
    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event)
        {
            PrimalUtilReg.registerItems(event, getItems());
        }
    }
}
