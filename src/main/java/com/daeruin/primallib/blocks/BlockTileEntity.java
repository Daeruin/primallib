package com.daeruin.primallib.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;

@SuppressWarnings("unused")
public abstract class BlockTileEntity<TE extends TileEntity> extends BlockBase
{

    public BlockTileEntity(String registryName, Material material, float hardness, boolean canBePickedUp)
    {
        super(registryName, material, hardness, canBePickedUp);
    }

    public abstract Class<TE> getTileEntityClass();

    public TE getTileEntity(IBlockAccess world, BlockPos pos)
    {
        //noinspection unchecked
        return (TE) world.getTileEntity(pos);
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public abstract TE createTileEntity(@Nullable World world, @Nullable IBlockState state);

}
