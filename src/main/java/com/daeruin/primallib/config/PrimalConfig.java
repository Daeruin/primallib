package com.daeruin.primallib.config;

import com.daeruin.primallib.PrimalLib;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = PrimalLib.MODID, category = "")
public class PrimalConfig
{
    private static final String LANG_KEY_PREFIX = "config." + PrimalLib.MODID + ":";
    private static final String LANG_KEY_PREFIX_BASICS = LANG_KEY_PREFIX + "basics";
    private static final String LANG_KEY_PREFIX_BARK = LANG_KEY_PREFIX + "bark_and_stripped_logs";
    private static final String LANG_KEY_PREFIX_BRANCHES = LANG_KEY_PREFIX + "branches_and_shafts";
    private static final String LANG_KEY_PREFIX_CORDAGE = LANG_KEY_PREFIX + "cordage";
    private static final String LANG_KEY_PREFIX_PLANT_FIBERS = LANG_KEY_PREFIX + "plant_fibers";
    private static final String LANG_KEY_PREFIX_TWIGS = LANG_KEY_PREFIX + "twigs";

    // Category tool tips (comments) are pulled from the lang file - that's why there's no @Config.Comment line for the category names

    @Config.Name("Basics")
    @Config.LangKey(LANG_KEY_PREFIX_BASICS)
    public static final Basics BASICS = new Basics();
    @Config.Name("Bark and Stripped Logs")
    @Config.LangKey(LANG_KEY_PREFIX_BARK)
    public static final BarkAndStrippedLogs BARK_AND_STRIPPED_LOGS = new BarkAndStrippedLogs();
    @Config.Name("Branches and Shafts")
    @Config.LangKey(LANG_KEY_PREFIX_BRANCHES)
    public static final BranchesAndShafts BRANCHES_AND_SHAFTS = new BranchesAndShafts();
    @Config.Name("Cordage")
    @Config.LangKey(LANG_KEY_PREFIX_CORDAGE)
    public static final Cordage CORDAGE = new Cordage();
    @Config.Name("Plant Fibers")
    @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS)
    public static final PlantFibers PLANT_FIBERS = new PlantFibers();
    @Config.Name("Twigs")
    @Config.LangKey(LANG_KEY_PREFIX_TWIGS)
    public static final Twigs TWIGS = new Twigs();

    public static class Basics
    {
        @Config.Name("Logs affect branch/twig drops")
        @Config.LangKey(LANG_KEY_PREFIX_BASICS + "logs_affect_branch_twig_drops")
        @Config.Comment("Branches only drop from leaves next to logs, and twigs only drop from leaves NOT next to logs.")
        public boolean branchTwigDependsOnLogProximity = true;
        @Config.Name("Cascading vine breakage")
        @Config.LangKey(LANG_KEY_PREFIX_BASICS + "cascading_vine_breakage")
        @Config.Comment("Whether breaking a vine block automatically breaks all attached vine blocks directly below.")
        public boolean cascadingVineBreakage = true;
        @Config.Name("Must sneak for mod drops")
        @Config.LangKey(LANG_KEY_PREFIX_BASICS + "must_sneak_for_mod_drops")
        @Config.Comment("Whether the player must be sneaking to get twigs, branches, and plant fibers. Set to true to give players more control over when they want to obtain these drops.")
        public boolean requireSneaking = false;
        @Config.Name("Enable Biomes O' Plenty integration")
        @Config.LangKey(LANG_KEY_PREFIX_BASICS + "enable_biomes_o_plenty_integration")
        @Config.Comment("When true, various Biomes O' Plenty grasses will drop plant fiber, and bushes and shrubs will drop twigs.")
        public boolean biomesOPlentyIntegration = false;
        @Config.Name("Enable No Tree Punching integration")
        @Config.LangKey(LANG_KEY_PREFIX_BASICS + "enable_no_tree_punching_integration")
        @Config.Comment("When true, hatchet and mattocks from No Tree Punching can get bark (but not planks) from a log, and saws can craft stripped logs into planks.")
        public boolean noTreePunchingIntegration = false;
    }

    public static class BarkAndStrippedLogs
    {
        @Config.Name("Allow bark and stripped logs")
        @Config.LangKey(LANG_KEY_PREFIX_BARK + "allow_bark_and_stripped_logs")
        @Config.Comment("Whether logs should produce bark when right-clicked wth an axe or put in the crafting grid with an axe. Disabling bark will also disable stripped logs and bark strips, and the other settings in this section will have no effect.")
        public boolean allowBarkAndStrippedLogs = true;
        @Config.Name("Craft bark strips from bark")
        @Config.LangKey(LANG_KEY_PREFIX_BARK + "craft_bark_strips_from_bark")
        @Config.Comment("Whether bark strips should be craftable from bark. Disabling bark strips will force Basket Case baskets to be made from vines or twigs, if they are enabled. Wood-specific Basket Case baskets require either bark strips or twigs.")
        public boolean allowBarkStrips = true;
        @Config.Name("Craft planks from stripped logs")
        @Config.LangKey(LANG_KEY_PREFIX_BARK + "craft_planks_from_stripped_logs")
        @Config.Comment("Whether stripped logs can be used to craft planks.")
        public boolean allowStrippedLogsToMakePlanks = true;
        @Config.Name("Plank recipe amount")
        @Config.LangKey(LANG_KEY_PREFIX_BARK + "plank_recipe_amount")
        @Config.Comment("How many planks are produced when crafting planks from stripped logs.")
        @Config.RangeInt(min = 0, max = 64)
        public int plankAmount = 4;
        @Config.Name("Bark amount")
        @Config.LangKey(LANG_KEY_PREFIX_BARK + "bark_amount")
        @Config.Comment("How many pieces of bark are produced when right-clicking logs with axe or when crafting from logs with axe.")
        @Config.RangeInt(min = 0, max = 64)
        public int barkAmount = 4;
        @Config.Name("Stripped log burn time")
        @Config.LangKey(LANG_KEY_PREFIX_BARK + "stripped_log_burn_time")
        @Config.Comment("How many ticks a stripped log will burn in a furnace.")
        public int strippedLogBurnTime = 300;
    }

    public static class BranchesAndShafts
    {
        // Branches
        @Config.Name("Allow branches from leaves")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "allow_branches_from_leaves")
        @Config.Comment("Whether branches should drop from leaves. Disabling branches will also disable wooden shafts, and the rest of the options in this section will have no effect.")
        // Add to comment when Darkest Before Dawn is done: , which will prevent Darkest Before Dawn campfires from being made unless you change sticksCountAsShafts to true.
        public boolean allowBranches = false;
        @Config.Name("Branch drop count")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "branch_drop_count")
        @Config.Comment("How many branches should drop when breaking leaves.")
        @Config.RangeInt(min = 0, max = 100)
        public int branchDropCount = 1;
        @Config.Name("Branch drop chance varies by tool")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "branch_drop_count_varies_by_tool")
        @Config.Comment("Whether the chance of getting branches should vary depending on the tool used. Setting this to false makes the next two settings have no effect.")
        public boolean branchDropChanceVariesByTool = true;
        @Config.Name("Branch drop chance with axe")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "branch_drop_chance_with_axe")
        @Config.Comment({"The chance that branches should drop when breaking leaves with an axe. Multiply by 100 to get % change, for example 0.2 = 20% chance.",
                "Min: 0.0", "Max: 1.0"})
        public float branchDropChanceWithAxe = 1.0F;
        @Config.Name("Branch drop chance with hands")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "branch_drop_chance_with_hands")
        @Config.Comment({"The chance that branches should drop when breaking leaves with your bare hands.",
                "Min: 0.0", "Max: 1.0"})
        public float branchDropChanceWithBareHands = 0.2F;
        // Wooden shafts
        @Config.Name("Allow wooden shafts")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "allow_wooden_shafts")
        @Config.Comment("Whether branches can be crafted into wooden shafts.")
        // Add to comment when Darkest Before Dawn is done: Wooden shafts are required to make Darkest Before Dawn campfires unless you change sticksCountAsShafts to true.
        public boolean allowWoodenShafts = true;
        @Config.Name("Sticks count as shafts")
        @Config.LangKey(LANG_KEY_PREFIX_BRANCHES + "sticks_count_as_shafts")
        @Config.Comment("Whether sticks can be used in place of shafts in crafting recipes. This setting doesn't have any practical use yet. It's for future functionality.")
        // Add to comment when Darkest Before Dawn is done: Set to true to allow Darkest Before Dawn campfires to be made with sticks instead of shafts.
        public boolean sticksCountAsShafts = false;
        // Remember that in 1.12 sticks can only be crafted. In 1.14 they start dropping from leaves and dead bushes.
    }

    public static class Cordage
    {
        @Config.Name("Allow bark strips as cordage")
        @Config.LangKey(LANG_KEY_PREFIX_CORDAGE + "allow_bark_strips_as_cordage")
        @Config.Comment("Whether bark strips can be used as cordage in crafting recipes. Cordage is used in Basket Case to make wicker baskets.")
        // Add to comment when Darkest Before Dawn is done: and in Darkest Before Dawn to make campfires
        public boolean allowBarkStripsAsCordage = true;
        @Config.Name("Allow plant fibers as cordage")
        @Config.LangKey(LANG_KEY_PREFIX_CORDAGE + "allow_plant_fibers_as_cordage")
        @Config.Comment("Whether cordage should be craftable from plant fibers. Cordage is used in Basket Case to make wicker baskets.")
        // Add to comment when Darkest Before Dawn is done: and in Darkest Before Dawn to make campfires
        public boolean allowPlantFibersAsCordage = true;
        @Config.Name("Allow string as cordage")
        @Config.LangKey(LANG_KEY_PREFIX_CORDAGE + "allow_string_as_cordage")
        @Config.Comment("Whether string can be used as cordage in crafting recipes. Cordage is used in Basket Case to make wicker baskets.")
        // Add to comment when Darkest Before Dawn is done: and in Darkest Before Dawn to make campfires
        public boolean allowStringAsCordage = false;
        @Config.Name("Allow sugar cane as cordage")
        @Config.LangKey(LANG_KEY_PREFIX_CORDAGE + "allow_sugar_cane_as_cordage")
        @Config.Comment("Whether sugar cane can be used as cordage in crafting recipes. Cordage is used in Basket Case to make wicker baskets.")
        // Add to comment when Darkest Before Dawn is done: and in Darkest Before Dawn to make campfires
        public boolean allowSugarCaneAsCordage = true;
        @Config.Name("Allow vines as cordage")
        @Config.LangKey(LANG_KEY_PREFIX_CORDAGE + "allow_vines_as_cordage")
        @Config.Comment("Whether vines can be used as cordage in crafting recipes. Cordage is used in Basket Case to make wicker baskets.")
        // Add to comment when Darkest Before Dawn is done: and in Darkest Before Dawn to make campfires
        public boolean allowVinesAsCordage = true;
    }

    public static class PlantFibers
    {
        @Config.Name("Allow plant fibers")
        @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS + "allow_plant_fibers")
        @Config.Comment("Whether plant fibers should drop from tall grass.")
        public boolean allowPlantFibers = true;
        @Config.Name("Plant fibers drop count")
        @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS + "plant_fibers_drop_count")
        @Config.Comment("How many plant fibers should drop when breaking tall grass.")
        @Config.RangeInt(min = 0, max = 100)
        public int plantFibersDropCount = 1;
        @Config.Name("Plant fiber drop chance varies by tool")
        @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS + "plant_fiber_drop_chance_varies_by_tool")
        @Config.Comment("Whether the chance of getting plant fibers should vary depending on the tool used. Setting this to false makes the next three settings have no effect.")
        public boolean plantFiberDropChanceVariesByTool = true;
        @Config.Name("Plant fiber drop chance with axe")
        @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS + "plant_fiber_drop_chance_with_axe")
        @Config.Comment({"The chance that plant fibers should drop when breaking tall grass with an axe.",
                "Min: 0.0", "Max: 1.0"})
        public float plantFibersDropChanceWithAxe = 1.0F;
        @Config.Name("Plant fiber drop chance with hands")
        @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS + "plant_fiber_drop_chance_with_hands")
        @Config.Comment({"The chance that plant fibers should drop when breaking tall grass with your bare hands.",
                "Min: 0.0", "Max: 1.0"})
        public float plantFibersDropChanceWithBareHands = 0.5F;
        @Config.Name("Plant fiber drop chance with shovel")
        @Config.LangKey(LANG_KEY_PREFIX_PLANT_FIBERS + "plant_fiber_drop_chance_with_shovel")
        @Config.Comment({"The chance that plant fibers should drop when breaking tall grass with a shovel.",
                "Min: 0.0", "Max: 1.0"})
        public float plantFibersDropChanceWithShovel = 0.7F;
    }

    public static class Twigs
    {
        @Config.Name("Allow twigs")
        @Config.LangKey(LANG_KEY_PREFIX_TWIGS + "allow_twigs")
        @Config.Comment("Whether twigs should drop from leaves. Disabling twigs will force Basket Case baskets to be made from vines or bark strips, if they are enabled. Wood-specific Basket Case baskets require either bark strips or twigs.")
        // Add to comment when Darkest Before Dawn is done: Darkest Before Dawn campfires require twigs.
        public boolean allowTwigs = true;
        @Config.Name("Twig drop count")
        @Config.LangKey(LANG_KEY_PREFIX_TWIGS + "twig_drop_count")
        @Config.Comment("How many twigs should drop when breaking leaves.")
        @Config.RangeInt(min = 0, max = 100)
        public int twigDropCount = 1;
        @Config.Name("Twig drop chance varies by tool")
        @Config.LangKey(LANG_KEY_PREFIX_TWIGS + "twig_drop_chance_varies_by_tool")
        @Config.Comment("Whether the chance of getting twigs should vary depending on the tool used. Setting this to false makes the next two settings have no effect.")
        public boolean twigDropChanceVariesByTool = true;
        @Config.Name("Twig drop chance with axe")
        @Config.LangKey(LANG_KEY_PREFIX_TWIGS + "twig_drop_chance_with_axe")
        @Config.Comment({"The chance that twigs should drop when breaking leaves with an axe.",
                "Min: 0.0", "Max: 1.0"})
        public float twigDropChanceWithAxe = 1.0F;
        @Config.Name("Twig drop chance with hands")
        @Config.LangKey(LANG_KEY_PREFIX_TWIGS + "twig_drop_chance_with_hands")
        @Config.Comment({"The chance that twigs should drop when breaking leaves with your bare hands.",
                "Min: 0.0", "Max: 1.0"})
        public float twigDropChanceWithBareHands = 0.5F;
    }

    @Mod.EventBusSubscriber
    private static class ConfigEventHandler
    {
        @SubscribeEvent
        public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
        {
            if (event.getModID().equals(PrimalLib.MODID))
            {
                ConfigManager.sync(PrimalLib.MODID, Config.Type.INSTANCE);
            }
        }
    }
}
