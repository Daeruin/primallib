package com.daeruin.primallib.recipes;

import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtilRecipe;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class OreDictRegistry
{
    public static void registerOreDictEntries()
    {
        // CORDAGE


        if (PrimalConfig.CORDAGE.allowPlantFibersAsCordage)
        {
            PrimalUtilRecipe.addAlternateIngredients("plantFiber", PrimalItemRegistry.PLANT_FIBER);
        }

        if (PrimalConfig.CORDAGE.allowVinesAsCordage)
        {
            PrimalUtilRecipe.addAlternateIngredients("cordageStrong", Blocks.VINE);
            PrimalUtilRecipe.addAlternateIngredients("cordage", Blocks.VINE);
        }

        if (PrimalConfig.CORDAGE.allowSugarCaneAsCordage)
        {
            PrimalUtilRecipe.addAlternateIngredients("plantFiber", Items.REEDS);
        }

        if (PrimalConfig.CORDAGE.allowBarkStripsAsCordage)
        {
            PrimalUtilRecipe.addAlternateIngredients("cordageStrong", PrimalItemRegistry.BARK_STRIPS_GENERIC);
            PrimalUtilRecipe.addAlternateIngredients("cordage", PrimalItemRegistry.BARK_STRIPS_GENERIC);
        }

        if (PrimalConfig.CORDAGE.allowStringAsCordage)
        {
            PrimalUtilRecipe.addAlternateIngredients("cordageWeak", Items.STRING);
            PrimalUtilRecipe.addAlternateIngredients("cordage", Items.STRING);
        }

        PrimalUtilRecipe.addAlternateIngredients("string", PrimalItemRegistry.TWINE);
        PrimalUtilRecipe.addAlternateIngredients("cordageWeak", PrimalItemRegistry.TWINE);
        PrimalUtilRecipe.addAlternateIngredients("cordage", PrimalItemRegistry.TWINE);
        PrimalUtilRecipe.addAlternateIngredients("twig", PrimalItemRegistry.TWIG_GENERIC);
        PrimalUtilRecipe.addAlternateIngredients("barkStrips", PrimalItemRegistry.BARK_STRIPS_GENERIC);

        // BARK

        PrimalUtilRecipe.addAlternateIngredients("barkWood", PrimalItemRegistry.OAK_BARK, PrimalItemRegistry.SPRUCE_BARK, PrimalItemRegistry.BIRCH_BARK, PrimalItemRegistry.JUNGLE_BARK, PrimalItemRegistry.ACACIA_BARK, PrimalItemRegistry.DARK_OAK_BARK);
        PrimalUtilRecipe.addAlternateIngredients("barkOak", PrimalItemRegistry.OAK_BARK);
        PrimalUtilRecipe.addAlternateIngredients("barkSpruce", PrimalItemRegistry.SPRUCE_BARK);
        PrimalUtilRecipe.addAlternateIngredients("barkBirch", PrimalItemRegistry.BIRCH_BARK);
        PrimalUtilRecipe.addAlternateIngredients("barkJungle", PrimalItemRegistry.JUNGLE_BARK);
        PrimalUtilRecipe.addAlternateIngredients("barkAcacia", PrimalItemRegistry.ACACIA_BARK);
        PrimalUtilRecipe.addAlternateIngredients("barkDarkOak", PrimalItemRegistry.DARK_OAK_BARK);

        // LOGS

        PrimalUtilRecipe.addAlternateIngredients("logWood", PrimalBlockRegistry.STRIPPED_OAK_LOG, PrimalBlockRegistry.STRIPPED_SPRUCE_LOG, PrimalBlockRegistry.STRIPPED_BIRCH_LOG, PrimalBlockRegistry.STRIPPED_JUNGLE_LOG, PrimalBlockRegistry.STRIPPED_ACACIA_LOG, PrimalBlockRegistry.STRIPPED_DARK_OAK_LOG);

        // STICKS & SHAFTS

        PrimalUtilRecipe.addAlternateIngredients("stickWood", PrimalItemRegistry.WOODEN_SHAFT);
        PrimalUtilRecipe.addAlternateIngredients("shaftWood", PrimalItemRegistry.WOODEN_SHAFT);

        if (PrimalConfig.BRANCHES_AND_SHAFTS.sticksCountAsShafts)
        {
            PrimalUtilRecipe.addAlternateIngredients("shaftWood", Items.STICK);
        }

        // BIOMES O' PLENTY

        if (Loader.isModLoaded("biomesoplenty") && PrimalConfig.BASICS.biomesOPlentyIntegration)
        {
            // To register a block with a meta value, create an ItemStack out of it
            // ItemStack itemStack = new ItemStack(block, 1, 4);
            // PrimalUtilRecipe.addAlternateIngredients("cordage", itemStack);

            // Get Forge's block registry
            IForgeRegistry<Block> blockRegistry = GameRegistry.findRegistry(Block.class);

            if (PrimalConfig.CORDAGE.allowVinesAsCordage)
            {
                // Get relevant Biomes O' Plenty block from the registry
                Block blockIvy = blockRegistry.getValue(new ResourceLocation("biomesoplenty:ivy"));

                // If the relevant block was found, register it to the OreDictionary with my term
                if (blockIvy != null)
                {
                    PrimalUtilRecipe.addAlternateIngredients("cordage", blockIvy);
                    PrimalUtilRecipe.addAlternateIngredients("cordageStrong", blockIvy);
                    PrimalUtilRecipe.addAlternateIngredients("vine", blockIvy);
                }

                Block blockWillowVine = blockRegistry.getValue(new ResourceLocation("biomesoplenty:willow_vine"));

                if (blockWillowVine != null)
                {
                    PrimalUtilRecipe.addAlternateIngredients("cordage", blockWillowVine);
                    PrimalUtilRecipe.addAlternateIngredients("cordageStrong", blockWillowVine);
                    PrimalUtilRecipe.addAlternateIngredients("vine", blockWillowVine);
                }
            }

            if (PrimalConfig.CORDAGE.allowSugarCaneAsCordage)
            {

                Block plant1 = blockRegistry.getValue(new ResourceLocation("biomesoplenty:plant_1"));

                if (plant1 != null)
                {
                    PrimalUtilRecipe.addAlternateIngredients("plantFiber", new ItemStack(plant1, 1, 4)); // Cattail
                    PrimalUtilRecipe.addAlternateIngredients("plantFiber", new ItemStack(plant1, 1, 5)); // River cane
                    PrimalUtilRecipe.addAlternateIngredients("plantFiber", new ItemStack(plant1, 1, 8)); // Reed
                    PrimalUtilRecipe.addAlternateIngredients("plantFiber", new ItemStack(plant1, 1, 9)); // Root
                }

                Block doublePlant = blockRegistry.getValue(new ResourceLocation("biomesoplenty:double_plant"));

                if (doublePlant != null)
                {
                    PrimalUtilRecipe.addAlternateIngredients("plantFiber", new ItemStack(doublePlant, 1, 0)); // Flax
                    PrimalUtilRecipe.addAlternateIngredients("plantFiber", new ItemStack(doublePlant, 1, 1)); // Tall cattail
                }
            }
        }
    }
}
