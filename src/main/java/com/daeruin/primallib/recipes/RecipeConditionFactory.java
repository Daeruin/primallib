package com.daeruin.primallib.recipes;

import com.daeruin.primallib.config.PrimalConfig;
import com.google.gson.JsonObject;
import net.minecraft.util.JsonUtils;
import net.minecraftforge.common.crafting.IConditionFactory;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.fml.common.Loader;

import java.util.function.BooleanSupplier;

@SuppressWarnings("unused")
public class RecipeConditionFactory implements IConditionFactory
{
    @Override
    public BooleanSupplier parse(JsonContext context, JsonObject json)
    {
        boolean allowCrafting;

        // Get the recipe name from the JSON that's being passed in
        // Expected JSON format:
        // json: {"type":"basketcase:is_recipe_allowed","primal_recipe_group":"large_basket"}
        String recipeName = JsonUtils.getString(json, "primal_recipe_group");

        // Figure out which recipe is being parsed, and get the config value for that recipe
        switch (recipeName)
        {
            // These recipes only use vanilla axes, which are disabled in NTP - no harm done to keep these recipes when NTP is loaded
            case "bark_from_log":
                allowCrafting = PrimalConfig.BARK_AND_STRIPPED_LOGS.allowBarkAndStrippedLogs;
                break;
            case "bark_from_log_NTP":
                // Recipes with this condition should only be loaded if No Tree Punching is installed
                allowCrafting = PrimalConfig.BARK_AND_STRIPPED_LOGS.allowBarkAndStrippedLogs && PrimalConfig.BASICS.noTreePunchingIntegration && Loader.isModLoaded("notreepunching");
                break;
            case "bark_strips_generic":
                allowCrafting = PrimalConfig.BARK_AND_STRIPPED_LOGS.allowBarkStrips;
                break;
            case "planks_from_stripped_logs":
                // No Tree Punching doesn't let logs make planks unless you have a saw
                // If No Tree Punching integration is on AND the mod is loaded, this recipe won't be allowed
                // If either the integration is off or the mod isn't loaded, the recipe will be allowed
                allowCrafting = PrimalConfig.BARK_AND_STRIPPED_LOGS.allowStrippedLogsToMakePlanks && (!PrimalConfig.BASICS.noTreePunchingIntegration || !Loader.isModLoaded("notreepunching"));
                break;
            case "planks_NTP":
                // These recipes are specific to No Tree Punching and should be loaded when planks are allowed and NTP is loaded
                allowCrafting = PrimalConfig.BARK_AND_STRIPPED_LOGS.allowStrippedLogsToMakePlanks && PrimalConfig.BASICS.noTreePunchingIntegration && Loader.isModLoaded("notreepunching");
                break;
            case "wooden_shaft":
                allowCrafting = PrimalConfig.BRANCHES_AND_SHAFTS.allowWoodenShafts;
                break;
            default:
                allowCrafting = true;
        }

        // This lambda stuff apparently turns a boolean into a BooleanSupplier
        return () -> allowCrafting;
    }
}
