package com.daeruin.primallib.util;

import com.daeruin.primallib.blocks.PrimalBlockLog;
import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.items.PrimalItemRegistry;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Loader;
import org.apache.commons.lang3.ArrayUtils;

import static net.minecraft.block.BlockLiquid.LEVEL;

public class PrimalUtil
{
    public static Item getItem(Block block)
    {
        return Item.getItemFromBlock(block);
    }

    /*
     *  GET BARK AND LOGS
     */

    public static BlockPlanks.EnumType getLogEnumType(IBlockState blockState)
    {
        Block block = blockState.getBlock();
        BlockPlanks.EnumType type = BlockPlanks.EnumType.OAK;

        if (block instanceof BlockLog)
        {
            if (block == Blocks.LOG)
            {
                type = blockState.getValue(BlockOldLog.VARIANT);
            }
            else if (block == Blocks.LOG2)
            {
                type = blockState.getValue(BlockNewLog.VARIANT);
            }
        }

        return type;
    }

    public static Block getStrippedLog(IBlockState blockState)
    {
        BlockPlanks.EnumType type = getLogEnumType(blockState);

        switch (type)
        {
            case ACACIA:
                return PrimalBlockRegistry.STRIPPED_ACACIA_LOG;
            case BIRCH:
                return PrimalBlockRegistry.STRIPPED_BIRCH_LOG;
            case DARK_OAK:
                return PrimalBlockRegistry.STRIPPED_DARK_OAK_LOG;
            case JUNGLE:
                return PrimalBlockRegistry.STRIPPED_JUNGLE_LOG;
            case OAK:
                return PrimalBlockRegistry.STRIPPED_OAK_LOG;
            case SPRUCE:
                return PrimalBlockRegistry.STRIPPED_SPRUCE_LOG;
        }

        return PrimalBlockRegistry.STRIPPED_OAK_LOG;
    }

    public static Item getBark(IBlockState blockState)
    {
        Block block = blockState.getBlock();

        if (block instanceof BlockLog)
        {
            BlockPlanks.EnumType type = getLogEnumType(blockState);

            switch (type)
            {
                case ACACIA:
                    return PrimalItemRegistry.ACACIA_BARK;
                case BIRCH:
                    return PrimalItemRegistry.BIRCH_BARK;
                case DARK_OAK:
                    return PrimalItemRegistry.DARK_OAK_BARK;
                case JUNGLE:
                    return PrimalItemRegistry.JUNGLE_BARK;
                case OAK:
                    return PrimalItemRegistry.OAK_BARK;
                case SPRUCE:
                    return PrimalItemRegistry.SPRUCE_BARK;
            }
        }

        return PrimalItemRegistry.OAK_BARK;
    }
    /*
     *  ADD DROPS TO HARVEST EVENT
     */

    public static void addDropWithChance(BlockEvent.HarvestDropsEvent event, Item dropItem, double chance, int amount)
    {
        double dieRoll = event.getWorld().rand.nextDouble();

        if (dieRoll <= chance)
        {
            event.getDrops().add(new ItemStack(dropItem, amount));
        }
    }

    /*
     *  EQUIVALENCY CHECKS
     */

    public static boolean isLog(Block block)
    {
        return block instanceof BlockLog || block instanceof PrimalBlockLog;
    }

    public static boolean isLog(World world, BlockPos pos)
    {
        Block block = world.getBlockState(pos).getBlock();
        return block instanceof BlockLog || block instanceof PrimalBlockLog;
    }

    public static boolean isAxe(Item item)
    {
        return (item instanceof ItemAxe || (PrimalConfig.BASICS.noTreePunchingIntegration && isNoTreePunchingAxe(item)));
    }

    private static boolean isNoTreePunchingAxe(Item item)
    {
        ResourceLocation resLoc = item.getRegistryName();

        if (resLoc != null)
        {
            String modName = resLoc.getNamespace();

            if (modName.equals("notreepunching"))
            {
                String itemName = resLoc.getPath();
                int indexOfSlashChar = itemName.indexOf('/');
                itemName = itemName.substring(0, indexOfSlashChar);

                return (itemName.equals("axe") || itemName.equals("mattock"));
            }
        }

        return false;
    }

    // Twigs + cordage = twig wicker
    // Bark strips = bark wicker
    // Vines = generic wicker
    // Generic bark strips = generic wicker
    // Generic twigs + cordage = generic wicker

    // Medium/Large wicker + bark strips = baskets
    // Medium/Large wicker + cordage = generic baskets

    // Biomes O' Plenty

    // biomesoplenty:plant_0
    // #4206/0 Short Grass      1 in 8 chance of getting a seed
    // #4206/1 Medium Grass     1 in 8 chance of getting a seed
    // #4206/2 Bush
    // #4206/3 Sprout           1 in 50 chance of getting a carrot or potato
    // #4206/4 Poison Ivy
    // #4206/5 Berry Bush       Always drops berries
    // #4206/6 Shrub
    // #4206/7 Wheat Grass      1 in 8 chance of getting a seed
    // #4206/8 Damp Grass       1 in 8 chance of getting a seed
    // #4206/9 Koru             Can make Koru Terrarium
    // #4206/10 Clover Patch
    // #4206/11 Leaf Pile
    // #4206/12 Dead Leaf Pile
    // #4206/13 Dead Grass
    // #4206/14 Desert Grass
    // #4206/15 Desert Sprouts

    // biomesoplenty:plant_1
    // #4207/0 Dune Grass
    // #4207/1 Spectral Fern    Can make Ender Terrarium
    // #4207/2 Thorns
    // #4207/3 Wild Rice        1 in 5 chance of dropping itself - Can make bowl of rice
    // #4207/4 Cattail          Always drops itself - Can make white wool and brown dye
    // #4207/5 River Cane       Always drops itself - Can be used in place of a stick in all recipes
    // #4207/6 Tiny Cactus      Always drops itself
    // #4207/7 Devilweed
    // #4207/8 Reed             Always drops itself
    // #4207/9 Root             Always drops itself
    // #4207/10 Rafflesia       Always drops itself
    // #4207/11 Barley          1 in 3 chance of getting a seed

    // biomesoplenty:double_plant
    // #4208/0 Flax             Can make Flax Terrarium and blue dye
    // #4208/1 Tall Cattail     Top and bottom drop cattails - Can make white wool and brown dye
    // #4208/2 Eyebulb          Bottom drops itself
    // #4208/3 Sea Oats         Can make Beach Terrarium

    // biomesoplenty:ivy
    // #4213/0 Ivy

    // biomesoplenty:willow_vine
    // #4214/0 Willow Vine

    public static boolean dropsPlantFiber(IBlockState blockState)
    {
        if (Loader.isModLoaded("biomesoplenty"))
        {
            Block block = blockState.getBlock();
            ResourceLocation resourceLocation = block.getRegistryName();
            int blockMeta = block.getMetaFromState(blockState);

            if (resourceLocation != null)
            {
                switch (resourceLocation.toString())
                {
                    // If this block's meta value is in the list of acceptable values for this block, it can drop plant fiber
                    case "biomesoplenty:plant_0":
                        int[] biomesOPlentyPlant0Metas = {1, 7, 13, 14}; // Medium grass, wheat grass, dead grass, desert grass
                        return ArrayUtils.contains(biomesOPlentyPlant0Metas, blockMeta);
                    case "biomesoplenty:plant_1":
                        int[] biomesOPlentyPlant1Metas = {0, 1, 3, 7, 11}; // Dune grass, spectral fern, wild rice, devilweed, barley
                        return ArrayUtils.contains(biomesOPlentyPlant1Metas, blockMeta);
                    case "biomesoplenty:double_plant":
                        int[] biomesOPlentyDoublePlantMetas = {3}; // Sea oats
                        return ArrayUtils.contains(biomesOPlentyDoublePlantMetas, blockMeta);
                }
            }
        }

        return false;
    }

    // This method is specific to Biomes O' Plenty bushes and shrubs
    public static boolean dropsTwigs(IBlockState blockState)
    {
        if (Loader.isModLoaded("biomesoplenty"))
        {
            Block block = blockState.getBlock();
            ResourceLocation resourceLocation = block.getRegistryName();

            if (resourceLocation != null && resourceLocation.toString().equals("biomesoplenty:plant_0"))
            {
                int[] biomesOPlentyPlant0Metas = {2, 6}; // Bush, shrub
                int blockMeta = block.getMetaFromState(blockState);

                return ArrayUtils.contains(biomesOPlentyPlant0Metas, blockMeta);
            }
        }

        return false;
    }

    public  static boolean isSlotHotbarOrOffhand(int slotIndex, IInventory inventory)
    {
        return (slotIndex < 9 || slotIndex == 40) && inventory instanceof InventoryPlayer;
    }

    public static boolean isItemStackInMainHand(EntityPlayer player, ItemStack stack)
    {
        return ItemStack.areItemStacksEqual(stack, player.getHeldItemMainhand());
    }

    /*
     *  Replacement of vanilla ray tracing method
     *  Fixes the stupid vanilla method that ignores flowing water/lava
     */

    @SuppressWarnings("ConstantConditions")
    public static RayTraceResult getRayTraceResultFromPlayer(World world, EntityLivingBase entity)
    {
        int reach = 4;
        float var4 = 1.0F;
        float pitch = entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * var4;
        float yaw = entity.prevRotationYaw + (entity.rotationYaw - entity.prevRotationYaw) * var4;
        double posX = entity.prevPosX + (entity.posX - entity.prevPosX) * var4;
        double posY = entity.prevPosY + (entity.posY - entity.prevPosY) * var4 + 1.62D - entity.getYOffset();
        double posZ = entity.prevPosZ + (entity.posZ - entity.prevPosZ) * var4;
        Vec3d vecOne = new Vec3d(posX, posY, posZ);

        float var14 = MathHelper.cos(-yaw * 0.017453292F - (float) Math.PI);
        float var15 = MathHelper.sin(-yaw * 0.017453292F - (float) Math.PI);
        float var16 = -MathHelper.cos(-pitch * 0.017453292F);
        float var17 = MathHelper.sin(-pitch * 0.017453292F);
        float var18 = var15 * var16;
        float var20 = var14 * var16;
        Vec3d vecTwo = vecOne.add(var18 * reach, var17 * reach, var20 * reach);

        boolean stopOnLiquid = true;
        boolean ignoreBlockWithoutBoundingBox = false;
        boolean returnLastUncollidableBlock = true;

        if (!Double.isNaN(vecOne.x) && !Double.isNaN(vecOne.y) && !Double.isNaN(vecOne.z))
        {
            if (!Double.isNaN(vecTwo.x) && !Double.isNaN(vecTwo.y) && !Double.isNaN(vecTwo.z))
            {
                int vecTwoX = MathHelper.floor(vecTwo.x);
                int vecTwoY = MathHelper.floor(vecTwo.y);
                int vecTwoZ = MathHelper.floor(vecTwo.z);
                int vecOneX = MathHelper.floor(vecOne.x);
                int vecOneY = MathHelper.floor(vecOne.y);
                int vecOneZ = MathHelper.floor(vecOne.z);
                BlockPos vecOnePos = new BlockPos(vecOneX, vecOneY, vecOneZ);
                IBlockState vecOneBlockState = world.getBlockState(vecOnePos);
                Block block = vecOneBlockState.getBlock();

                if ((!ignoreBlockWithoutBoundingBox || vecOneBlockState.getCollisionBoundingBox(world, vecOnePos) != Block.NULL_AABB) && block.canCollideCheck(vecOneBlockState, stopOnLiquid))
                {
                    RayTraceResult raytraceresult = vecOneBlockState.collisionRayTrace(world, vecOnePos, vecOne, vecTwo);

                    if (raytraceresult != null)
                    {
                        return raytraceresult;
                    }
                }

                RayTraceResult raytraceresult2 = null;
                int k1 = 200;

                while (k1-- >= 0)
                {
                    if (Double.isNaN(vecOne.x) || Double.isNaN(vecOne.y) || Double.isNaN(vecOne.z))
                    {
                        return null;
                    }

                    if (vecOneX == vecTwoX && vecOneY == vecTwoY && vecOneZ == vecTwoZ)
                    {
                        return returnLastUncollidableBlock ? raytraceresult2 : null;
                    }

                    boolean flag2 = true;
                    boolean flag = true;
                    boolean flag1 = true;
                    double d0 = 999.0D;
                    double d1 = 999.0D;
                    double d2 = 999.0D;

                    if (vecTwoX > vecOneX)
                    {
                        d0 = (double) vecOneX + 1.0D;
                    }
                    else if (vecTwoX < vecOneX)
                    {
                        d0 = (double) vecOneX + 0.0D;
                    }
                    else
                    {
                        flag2 = false;
                    }

                    if (vecTwoY > vecOneY)
                    {
                        d1 = (double) vecOneY + 1.0D;
                    }
                    else if (vecTwoY < vecOneY)
                    {
                        d1 = (double) vecOneY + 0.0D;
                    }
                    else
                    {
                        flag = false;
                    }

                    if (vecTwoZ > vecOneZ)
                    {
                        d2 = (double) vecOneZ + 1.0D;
                    }
                    else if (vecTwoZ < vecOneZ)
                    {
                        d2 = (double) vecOneZ + 0.0D;
                    }
                    else
                    {
                        flag1 = false;
                    }

                    double d3 = 999.0D;
                    double d4 = 999.0D;
                    double d5 = 999.0D;
                    double d6 = vecTwo.x - vecOne.x;
                    double d7 = vecTwo.y - vecOne.y;
                    double d8 = vecTwo.z - vecOne.z;

                    if (flag2)
                    {
                        d3 = (d0 - vecOne.x) / d6;
                    }

                    if (flag)
                    {
                        d4 = (d1 - vecOne.y) / d7;
                    }

                    if (flag1)
                    {
                        d5 = (d2 - vecOne.z) / d8;
                    }

                    if (d3 == -0.0D)
                    {
                        d3 = -1.0E-4D;
                    }

                    if (d4 == -0.0D)
                    {
                        d4 = -1.0E-4D;
                    }

                    if (d5 == -0.0D)
                    {
                        d5 = -1.0E-4D;
                    }

                    EnumFacing enumfacing;

                    if (d3 < d4 && d3 < d5)
                    {
                        enumfacing = vecTwoX > vecOneX ? EnumFacing.WEST : EnumFacing.EAST;
                        vecOne = new Vec3d(d0, vecOne.y + d7 * d3, vecOne.z + d8 * d3);
                    }
                    else if (d4 < d5)
                    {
                        enumfacing = vecTwoY > vecOneY ? EnumFacing.DOWN : EnumFacing.UP;
                        vecOne = new Vec3d(vecOne.x + d6 * d4, d1, vecOne.z + d8 * d4);
                    }
                    else
                    {
                        enumfacing = vecTwoZ > vecOneZ ? EnumFacing.NORTH : EnumFacing.SOUTH;
                        vecOne = new Vec3d(vecOne.x + d6 * d5, vecOne.y + d7 * d5, d2);
                    }

                    vecOneX = MathHelper.floor(vecOne.x) - (enumfacing == EnumFacing.EAST ? 1 : 0);
                    vecOneY = MathHelper.floor(vecOne.y) - (enumfacing == EnumFacing.UP ? 1 : 0);
                    vecOneZ = MathHelper.floor(vecOne.z) - (enumfacing == EnumFacing.SOUTH ? 1 : 0);
                    vecOnePos = new BlockPos(vecOneX, vecOneY, vecOneZ);
                    IBlockState iblockstate1 = world.getBlockState(vecOnePos);
                    Block block1 = iblockstate1.getBlock();

                    if (!ignoreBlockWithoutBoundingBox || iblockstate1.getMaterial() == Material.PORTAL || iblockstate1.getCollisionBoundingBox(world, vecOnePos) != Block.NULL_AABB)
                    {
                        if (block1.canCollideCheck(iblockstate1, stopOnLiquid) || (block1 instanceof BlockLiquid && iblockstate1.getValue(LEVEL) != 0))
                        {
                            RayTraceResult raytraceresult1 = iblockstate1.collisionRayTrace(world, vecOnePos, vecOne, vecTwo);

                            if (raytraceresult1 != null)
                            {
                                return raytraceresult1;
                            }
                        }
                        else
                        {
                            raytraceresult2 = new RayTraceResult(RayTraceResult.Type.MISS, vecOne, enumfacing, vecOnePos);
                        }
                    }
                }

                return returnLastUncollidableBlock ? raytraceresult2 : null;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
}
