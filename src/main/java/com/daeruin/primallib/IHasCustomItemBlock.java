package com.daeruin.primallib;

import net.minecraft.item.ItemBlock;

public interface IHasCustomItemBlock
{
    ItemBlock getItemBlock();
}
