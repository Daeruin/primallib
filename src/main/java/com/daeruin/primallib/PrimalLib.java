package com.daeruin.primallib;

import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.events.PrimalEventHandler;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.proxy.IProxy;
import com.daeruin.primallib.recipes.OreDictRegistry;
import com.daeruin.primallib.util.PrimalCreativeTab;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = PrimalLib.MODID,
        name = "PrimalLib",
        version = "{@version}",
        dependencies = "before:basketcase;before:darkestbeforedawn",
        acceptedMinecraftVersions = "1.12, 1.12.1, 1.12.2"
)

public class PrimalLib
{
    public static final String MODID = "primallib";

    @SuppressWarnings("CanBeFinal")
    @Mod.Instance
    public static PrimalLib INSTANCE = new PrimalLib();

    @SidedProxy(modId = MODID, clientSide = "com.daeruin.primallib.proxy.ClientProxy", serverSide = "com.daeruin.primallib.proxy.ServerProxy")
    private static IProxy proxy;

    public static final Item.ToolMaterial PRIMAL_WOOD = EnumHelper.addToolMaterial("PRIMAL_WOOD", 0, 11, 1.0F, 1.0F, 0);

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
        // NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
        proxy.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e)
    {
        OreDictRegistry.registerOreDictEntries();
        // This only needed to be run one time to create the recipes - only uncomment again if the recipes need to be changed (unlikely since No Tree Punching for 1.12 has been abandoned)
        // PrimalUtilRecipe.generateNoTreePunchingRecipes();
        PrimalCreativeTab.addBlocksToSortOrder(PrimalBlockRegistry.getBlocks());
        PrimalCreativeTab.addItemsToSortOrder(PrimalItemRegistry.getItems());
        proxy.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e)
    {
        MinecraftForge.EVENT_BUS.register(new PrimalEventHandler());
        proxy.postInit();
    }
}
