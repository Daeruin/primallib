package com.daeruin.primallib.util;

import com.daeruin.primallib.recipes.DummyRecipe;
import com.daeruin.primallib.recipes.ParsedPrimalRecipe;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPlanks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

public class PrimalUtilRecipe
{
    // Registers items and blocks with Forge OreDictionary
    public static void addAlternateIngredients(String name, Object... objects)
    {
        for (Object object : objects)
        {
            if (object instanceof Block)
            {
                OreDictionary.registerOre(name, (Block) object);
            }
            else if (object instanceof Item)
            {
                OreDictionary.registerOre(name, (Item) object);
            }
            else if (object instanceof ItemStack)
            {
                OreDictionary.registerOre(name, (ItemStack) object);
            }
            else
            {
                FMLLog.bigWarning(
                        "Invalid registration attempt for an Ore Dictionary item with name {} has occurred. The registration has been denied to prevent crashes. The mod responsible for the registration needs to correct this.",
                        name);
            }
        }
    }

    // Removes any crafting recipe whose output matches the given item
    public static void removeCraftingRecipe(Item itemToMatch)
    {
        IForgeRegistry<IRecipe> recipeRegistry = ForgeRegistries.RECIPES;

        for (IRecipe recipe : recipeRegistry)
        {
            ItemStack recipeOutput = recipe.getRecipeOutput();

            // If the recipe output matches the item we're checking
            if (!recipeOutput.isEmpty() && recipeOutput.getItem() == itemToMatch && recipe.getRegistryName() != null)
            {
                System.out.println("\033[0;33m" + "Please ignore 'Potentially Dangerous alternative prefix' warning for recipe `" + itemToMatch.getRegistryName() + "`. It's being removed intentionally due to a config setting.");
                IRecipe replacement = new DummyRecipe().setRegistryName(recipe.getRegistryName());
                recipeRegistry.register(replacement);
            }
        }
    }

    public static void removeCraftingRecipe(Block block)
    {
        removeCraftingRecipe(Item.getItemFromBlock(block));
    }

    // Removes any smelting recipe whose output matches the given item
    public static void removeSmeltingRecipe(Item itemToMatch)
    {
        Map<ItemStack, ItemStack> recipeList = FurnaceRecipes.instance().getSmeltingList();
        Iterator<Map.Entry<ItemStack, ItemStack>> recipeIterator = recipeList.entrySet().iterator();

        while (recipeIterator.hasNext())
        {
            Map.Entry<ItemStack, ItemStack> entry = recipeIterator.next();
            ItemStack recipeOutput = entry.getValue();

            if (!recipeOutput.isEmpty() && recipeOutput.getItem() == itemToMatch) // If the recipe output matches the item we're checking
            {
                recipeIterator.remove(); // Remove the recipe
            }
        }
    }

    // Parse a JSON recipe for Resource Location, ingredients, and result
    // Put them in a ParsedPrimalRecipe object and return it
    // Ultimately the recipe will end up in a PrimalRecipe, which is an extension of ShapelessOreRecipe with a custom getRemainingItems method
    // There's no real reason for this method to exist outside of the PrimalRecipeFactory unless I someday create other recipe factories that need to parse recipes
    public static ParsedPrimalRecipe parsePrimalRecipe(final JsonContext context, final JsonObject json)
    {
        // Get recipe group (can group recipes together in the recipe interface, for example all door recipes)
        String group = JsonUtils.getString(json, "group", "");

        // Get ingredients
        NonNullList<Ingredient> ingredients = NonNullList.create();

        for (JsonElement element : JsonUtils.getJsonArray(json, "ingredients"))
        {
            ingredients.add(CraftingHelper.getIngredient(element, context));
        }

        if (ingredients.isEmpty())
        {
            throw new JsonParseException("No ingredients for shapeless recipe");
        }

        // Get crafting result
        ItemStack result = CraftingHelper.getItemStack(JsonUtils.getJsonObject(json, "result"), context);

        // Get recipe group - determines logic for RecipeConditionFactory and recipe tweaking in PrimalRecipe
        String primalRecipeGroup = json.getAsJsonArray("conditions").get(0).getAsJsonObject().get("primal_recipe_group").getAsString();

        return new ParsedPrimalRecipe(group.isEmpty() ? null : new ResourceLocation(group), ingredients, result, primalRecipeGroup);
    }

    // Generates recipes for compatibility with No Tree Punching
    // Usually disabled since this was a one-time issue - will only need to run again if I need to make a change to the recipes
    @SuppressWarnings("unused")
    public static void generateNoTreePunchingRecipes()
    {
        String NTP_MOD_ID = "notreepunching";
        String ASSETS_DIR = "/Users/" + System.getProperty("user.name") + "/Developer/mc112/primallib112/src/main/resources/assets/primallib/recipes/";

        // Make recipes for axe + log = bark + stripped log

        String NTP_AXES[] = {
                "axe/flint",
                "mattock/iron",
                "mattock/gold",
                "mattock/diamond",
                "mattock/copper",
                "mattock/tin",
                "mattock/bronze",
                "mattock/steel"
        };

        // Iterate through all the axes listed above, then all the wood types, to generate a recipe for each axe and wood type combination
        for (String axe : NTP_AXES)
        {
            for (BlockPlanks.EnumType woodType : BlockPlanks.EnumType.values())
            {
                // Define the recipe name and file path
                String woodName = woodType.getName();
                String recipeFileName = "NTP_bark_" + woodName + "_from_log_with_" + axe.replace('/', '_') + ".json";
                String recipeText = constructBarkRecipeText(NTP_MOD_ID, axe, woodType);

                writeRecipe(recipeFileName, recipeText, ASSETS_DIR);
            }
        }

        // Make recipes for saw + stripped log = planks

        String NTP_SAWS[] = {
                "saw/iron",
                "saw/gold",
                "saw/diamond",
                "saw/copper",
                "saw/tin",
                "saw/bronze",
                "saw/steel"
        };

        // Iterate through all the saws listed above, then all the wood types, to generate a recipe for each saw and wood type combination
        for (String saw : NTP_SAWS)
        {
            for (BlockPlanks.EnumType woodType : BlockPlanks.EnumType.values())
            {
                // Define the recipe name and file path
                String woodName = woodType.getName();
                String recipeFileName = "NTP_planks_" + woodName + "_with_" + saw.replace('/', '_') + ".json";
                String recipeText = constructSawRecipeText(NTP_MOD_ID, saw, woodType);

                writeRecipe(recipeFileName, recipeText, ASSETS_DIR);
            }
        }
    }

    private static String constructBarkRecipeText(String modid, String tool, BlockPlanks.EnumType woodType)
    {
        int logMeta = woodType.getMetadata();
        String woodName = woodType.getName();
        String logType = "log";

        if (woodName.equals("acacia") || woodName.equals("dark_oak"))
        {
            logType = "log2";
            // This hard-coded calculation turns the BlockPlanks.EnumType into the BlockNewLog's meta value
            // The same calculation can be seen in BlockNewLog#getMetaFromState
            // BlockNewLog (aka log2) has meta of 0 for acacia and 1 for dark_oak
            logMeta -= 4;
        }

        return "{\n" +
                "  \"conditions\": [\n" +
                "    {\n" +
                "      \"type\": \"primallib:is_recipe_allowed\",\n" +
                "      \"recipe_name\": \"bark_from_log_NTP\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"type\": \"primallib:primallib_recipe\",\n" +
                "  \"ingredients\": [\n" +
                "    {\n" +
                "      \"item\": \"" + modid + ":" + tool + "\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"item\": \"minecraft:" + logType + "\",\n" +
                "      \"data\": " + logMeta + "\n" +
                "    }\n" +
                "  ],\n" +
                "  \"result\": {\n" +
                "    \"item\": \"primallib:bark_" + woodName + "\",\n" +
                "    \"count\": 4\n" +
                "  }\n" +
                "}";
    }

    private static String constructSawRecipeText(String modid, String tool, BlockPlanks.EnumType woodType)
    {
        int logMeta = woodType.getMetadata();
        String woodName = woodType.getName();

        return "{\n" +
                "  \"conditions\":\n" +
                "  [\n" +
                "    {\n" +
                "      \"type\": \"primallib:is_recipe_allowed\",\n" +
                "      \"recipe_name\": \"planks_NTP\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"type\": \"forge:ore_shaped\",\n" +
                "  \"pattern\":\n" +
                "  [\n" +
                "    \"S\",\n" +
                "    \"L\"\n" +
                "  ],\n" +
                "  \"key\":\n" +
                "  {\n" +
                "    \"S\":\n" +
                "    {\n" +
                "      \"item\": \"" + modid + ":" + tool + "\",\n" +
                "       \"data\": 32767\n" +
                "    },\n" +
                "    \"L\":\n" +
                "    {\n" +
                "      \"item\": \"primallib:log_stripped_" + woodName + "\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"result\": {\n" +
                "    \"item\": \"minecraft:planks\",\n" +
                "    \"data\": " + logMeta + ",\n" +
                "    \"count\": 4\n" +
                "  }\n" +
                "}";
    }

    // Write a recipe file to the file system
    private static void writeRecipe(String recipeFileName, String recipeText, String recipeDestination)
    {
        Path path = Paths.get(recipeDestination + recipeFileName);

        // Put the recipe into an array with each line in its own entry
        ArrayList<String> recipeIterable = new ArrayList<>(Arrays.asList(recipeText.split("/n")));

        // path is directory path and file name of recipe file
        // recipeIterable is an Iterable containing the recipe text with each line in one entry of the Iterable
        try
        {
            Files.write(path, recipeIterable, Charset.forName("UTF-8"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
