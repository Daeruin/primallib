package com.daeruin.primallib.recipes;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

// This is just an intermediary object to hold the results of a recipe factory like PrimalRecipeFactory
// before passing it into an IRecipe implementation like PrimalRecipe
public class ParsedPrimalRecipe
{
    private ResourceLocation resourceLocation;
    private NonNullList<Ingredient> ingredientList;
    private ItemStack craftingResult;
    private String recipeType;

    public ParsedPrimalRecipe(ResourceLocation resourceLocation, NonNullList<Ingredient> ingredientList, @Nonnull ItemStack craftingResult, String recipeType)
    {
        this.resourceLocation = resourceLocation;
        this.ingredientList = ingredientList;
        this.craftingResult = craftingResult.copy();
        this.recipeType = recipeType;
    }

    public ResourceLocation getResourceLocation()
    {
        return this.resourceLocation;
    }

    public NonNullList<Ingredient> getIngredientList()
    {
        return this.ingredientList;
    }

    public ItemStack getCraftingResult()
    {
        return craftingResult;
    }

    public String getRecipeType()
    {
        return recipeType;
    }
}