package com.daeruin.primallib.util;

import com.daeruin.primallib.IHasCustomItemBlock;
import com.daeruin.primallib.INeedsCustomModelReg;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.MissingMappings.Mapping;
import net.minecraftforge.registries.IForgeRegistry;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class PrimalUtilReg
{
    /*
     *  SET REGISTRY NAMES AND CREATIVE TABS
     */

    public static void initializeBlock(Block block, String name)
    {
        block.setCreativeTab(PrimalCreativeTab.PRIMAL_TAB);
        block.setRegistryName(name);

        if (block.getRegistryName() != null)
        {
            block.setTranslationKey(block.getRegistryName().toString());
        }
    }

    public static void initializeItem(Item item, String name)
    {
        item.setCreativeTab(PrimalCreativeTab.PRIMAL_TAB);
        item.setRegistryName(name);

        if (item.getRegistryName() != null)
        {
            item.setTranslationKey(item.getRegistryName().toString());
        }
    }

    /*
     *  REGISTER BLOCKS AND ITEMS
     */

    public static void registerItems(RegistryEvent.Register<Item> event, LinkedHashSet<Item> items)
    {
        IForgeRegistry<Item> itemRegistry = event.getRegistry();

        for (Item item : items)
        {
            itemRegistry.register(item);
        }
    }

    public static void registerBlocks(RegistryEvent.Register<Block> event, LinkedHashSet<Block> blocks)
    {
        IForgeRegistry<Block> blockRegistry = event.getRegistry();

        for (Block block : blocks)
        {
            blockRegistry.register(block);
        }
    }

    public static void registerItemBlocks(RegistryEvent.Register<Item> event, HashSet<Block> blocks)
    {
        IForgeRegistry<Item> itemBlockRegistry = event.getRegistry();

        for (Block block : blocks)
        {
            ItemBlock itemBlock;

            if (block instanceof IHasCustomItemBlock)
            {
                itemBlock = ((IHasCustomItemBlock) block).getItemBlock();
            }
            else
            {
                itemBlock = new ItemBlock(block);
            }

            if (block.getRegistryName() != null)
            {
                itemBlock.setRegistryName(block.getRegistryName());
                itemBlockRegistry.register(itemBlock);
            }
        }
    }

    /*
     *  REGISTER MODELS
     */

    public static void registerItemModels(HashSet<Item> items)
    {
        for (Item item : items)
        {
            if (item instanceof INeedsCustomModelReg)
            {
                ((INeedsCustomModelReg) item).registerCustomModel();
            }
            else
            {
                registerModel(item, 0, getResourceLocation(item)); // assumes the inventory variant
            }
        }
    }

    public static void registerBlockModels(HashSet<Block> blocks)
    {
        for (Block block : blocks)
        {
            if (block instanceof INeedsCustomModelReg)
            {
                ((INeedsCustomModelReg) block).registerCustomModel();
            }
            else
            {
                Item item = PrimalUtil.getItem(block);
                registerModel(item, 0, getResourceLocation(item)); // assumes the inventory variant
            }
        }
    }

    // Main registration methods

    // private static void registerItemModel(Item item)
    // {
    //     registerModel(item, 0, getResourceLocation(item)); // assumes the inventory variant
    // }

    // public static void registerBlockModel(Block block)
    // {
    //     Item item = getItem(block);
    //     registerModel(item, 0, getResourceLocation(item)); // assumes the inventory variant
    // }

    // Helper methods

    public static ModelResourceLocation getResourceLocation(Item item)
    {
        //noinspection ConstantConditions
        return new ModelResourceLocation(item.getRegistryName().toString(), "inventory");
    }

    // Actual registration

    public static void registerModel(Item item, int meta, ModelResourceLocation resourceLocation)
    {
        ModelLoader.setCustomModelResourceLocation(item, meta, resourceLocation);
    }

    /*
     *  REMAP BLOCKS AND ITEMS
     */

    public static void remapBlockTo(Mapping<Block> mapping, String newName, LinkedHashSet<Block> blockList)
    {
        // System.out.println("Mapping key is one we need to remap - attempting to remap now");

        ResourceLocation newRegistryName = new ResourceLocation(newName);
        Block newBlock = getRegisteredBlock(newRegistryName, blockList);

        // System.out.println("Matching block is: " + newBlock);

        if (newBlock != null)
        {
            // System.out.println("Found " + newRegistryName + " in BasketCase registry - remapping");
            mapping.remap(newBlock);
            // System.out.println("Mapping target after remapping: " + mapping.getTarget());
        }
        else
        {
            // System.out.println("Failed to find matching block");
            mapping.fail();
        }
    }

    // Utility method to get any registered block from a LinkedHashSet - because I wanted the blocks themselves to remain private
    private static Block getRegisteredBlock(ResourceLocation blockName, LinkedHashSet<Block> blockList)
    {
        for (Block block : blockList)
        {
            //noinspection ConstantConditions
            if (block.getRegistryName().compareTo(blockName) == 0)
            {
                return block;
            }
        }

        return null;
    }

    public static void remapItemTo(Mapping<Item> mapping, String newName, LinkedHashSet<Item> itemList)
    {
        // System.out.println("Mapping key is one we need to remap - attempting to remap now");

        ResourceLocation newRegistryName = new ResourceLocation(newName);
        Item newItem = getRegisteredItem(newRegistryName, itemList);

        // System.out.println("Matching item is: " + newItem);

        if (newItem != null)
        {
            // System.out.println("Found " + newRegistryName + " in BasketCase registry - remapping");
            mapping.remap(newItem);
            // System.out.println("Mapping target after remapping: " + mapping.getTarget());
        }
        else
        {
            // System.out.println("Failed to find matching item");
            mapping.fail();
        }
    }

    // Utility method to get any registered block from a LinkedHashSet - because I wanted the blocks themselves to remain private
    private static Item getRegisteredItem(ResourceLocation itemName, LinkedHashSet<Item> itemList)
    {
        for (Item item : itemList)
        {
            //noinspection ConstantConditions
            if (item.getRegistryName().compareTo(itemName) == 0)
            {
                return item;
            }
        }

        return null;
    }

    public static void remapItemBlockTo(Mapping<Item> mapping, String newName, LinkedHashSet<Block> blockList)
    {
        // System.out.println("Mapping key is one we need to remap - attempting to remap now");

        ResourceLocation newRegistryName = new ResourceLocation(newName);
        Block newBlock = getRegisteredBlock(newRegistryName, blockList);

        // System.out.println("Matching item is: " + newBlock);

        if (newBlock != null)
        {
            // System.out.println("Found " + newRegistryName + " in BasketCase registry - remapping");
            Item itemBlock = Item.getItemFromBlock(newBlock);
            mapping.remap(itemBlock);
            // System.out.println("Mapping target after remapping: " + mapping.getTarget());
        }
        else
        {
            // System.out.println("Failed to find matching item");
            mapping.fail();
        }
    }
}
