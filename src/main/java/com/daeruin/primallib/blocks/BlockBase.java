package com.daeruin.primallib.blocks;

import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockBase extends Block
{
    private final boolean canBePickedUp;

    protected BlockBase(String registryName, Material material, boolean canBePickedUp)
    {
        super(material);
        PrimalUtilReg.initializeBlock(this, registryName);
        this.canBePickedUp = canBePickedUp;
    }

    BlockBase(String registryName, Material material, float hardness, boolean canBePickedUp)
    {
        this(registryName, material, canBePickedUp);
        this.setHardness(hardness);
    }

    private boolean canBePickedUp()
    {
        return canBePickedUp;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (player.isSneaking() && canBePickedUp())
        {
            pickUpBlock(world, pos, new ItemStack(this)); // Default is to drop the current block as-is; can be set to drop a different ItemStack by overriding pickUpBlock, creating a different ItemStack, and passing it to super.pickUpBlock
        }
        else
        {
            return useBlock(world, pos, player);
        }

        return false;
    }

    public void pickUpBlock(World world, BlockPos pos, ItemStack itemStack)
    {
        spawnAsEntity(world, pos, itemStack);
        world.setBlockToAir(pos);
    }

    public boolean useBlock(World world, BlockPos pos, EntityPlayer player)
    {
        return true; // Override to return false to allow the held item's onItemUse method to run
    }
}
