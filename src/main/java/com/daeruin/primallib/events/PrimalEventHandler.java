package com.daeruin.primallib.events;

import com.daeruin.primallib.blocks.PrimalBlockLog;
import com.daeruin.primallib.config.PrimalConfig;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtil;
import com.daeruin.primallib.util.PrimalUtilSpawn;
import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;
import java.util.Objects;

public class PrimalEventHandler
{
    @SubscribeEvent
    public void onHarvestDropsEvent(BlockEvent.HarvestDropsEvent event)
    {
        if (event.getHarvester() == null)
        {
            return;
        }

        EntityPlayer player = event.getHarvester();

        if (!PrimalConfig.BASICS.requireSneaking || player.isSneaking())
        {
            ItemStack heldItemStack = player.getHeldItemMainhand();
            Item heldItem = heldItemStack.getItem();
            IBlockState blockState = event.getState();
            Block block = blockState.getBlock();
            float chance = 0.0F;

            // TALL GRASS --> PLANT FIBERS

            if (PrimalConfig.PLANT_FIBERS.allowPlantFibers)
            {
                // dropsPlantFiber checks if the block is a certain variety of Biomes O' Plenty grass
                if (block instanceof BlockTallGrass || block instanceof BlockDoublePlant || (PrimalConfig.BASICS.biomesOPlentyIntegration && PrimalUtil.dropsPlantFiber(blockState)))
                {
                    if (PrimalConfig.PLANT_FIBERS.plantFiberDropChanceVariesByTool)
                    {
                        if (heldItemStack.isEmpty())
                        {
                            chance = PrimalConfig.PLANT_FIBERS.plantFibersDropChanceWithBareHands;
                        }
                        else if (heldItem instanceof ItemSpade)
                        {
                            chance = PrimalConfig.PLANT_FIBERS.plantFibersDropChanceWithShovel;
                        }
                        else if (PrimalUtil.isAxe(heldItem))
                        {
                            chance = PrimalConfig.PLANT_FIBERS.plantFibersDropChanceWithAxe;
                        }
                    }
                    else
                    {
                        chance = 1.0F;
                    }

                    if (chance > 0.0F)
                    {
                        PrimalUtil.addDropWithChance(event, PrimalItemRegistry.PLANT_FIBER, chance, PrimalConfig.PLANT_FIBERS.plantFibersDropCount);
                    }
                }
            }

            // LEAVES --> TWIGS or BRANCHES

            if (block instanceof BlockLeaves && (PrimalConfig.TWIGS.allowTwigs || PrimalConfig.BRANCHES_AND_SHAFTS.allowBranches))
            {
                boolean leafIsNextToLog = false;

                if (PrimalConfig.BASICS.branchTwigDependsOnLogProximity)
                {
                    BlockPos posOrigin = event.getPos();

                    for (EnumFacing direction : EnumFacing.VALUES)
                    {
                        if (PrimalUtil.isLog(event.getWorld(), posOrigin.offset(direction)))
                        {
                            leafIsNextToLog = true;
                        }
                    }
                }

                if (PrimalConfig.TWIGS.allowTwigs && (!PrimalConfig.BASICS.branchTwigDependsOnLogProximity || !leafIsNextToLog))
                {
                    chance = getTwigDropChance(heldItemStack);

                    if (chance > 0.0F)
                    {
                        Item itemToDrop = PrimalItemRegistry.TWIG_GENERIC;
                        // If Basket Case installed, its harvest event will change this to a wood-specific twig if wood-specific baskets are enabled

                        PrimalUtil.addDropWithChance(event, itemToDrop, chance, PrimalConfig.TWIGS.twigDropCount);
                    }
                }

                if (PrimalConfig.BRANCHES_AND_SHAFTS.allowBranches && (!PrimalConfig.BASICS.branchTwigDependsOnLogProximity || leafIsNextToLog))
                {
                    chance = getBranchDropChance(heldItemStack);

                    if (chance > 0.0F)
                    {
                        PrimalUtil.addDropWithChance(event, PrimalItemRegistry.BRANCH, chance, PrimalConfig.BRANCHES_AND_SHAFTS.branchDropCount);
                    }
                }
            }

            // BIOMES O' PLENTY BUSH OR SHRUB --> TWIGS

            // dropsTwigs checks if the block is a Biomes O' Plenty bush or shrub
            if (PrimalUtil.dropsTwigs(blockState) && PrimalConfig.TWIGS.allowTwigs && PrimalConfig.BASICS.biomesOPlentyIntegration)
            {
                chance = getTwigDropChance(heldItemStack);

                if (chance > 0.0F)
                {
                    Item itemToDrop = PrimalItemRegistry.TWIG_GENERIC;
                    // If Basket Case is installed, its harvest event will change this to an oak twig if wood-specific baskets are enabled

                    PrimalUtil.addDropWithChance(event, itemToDrop, chance, PrimalConfig.TWIGS.twigDropCount);
                }
            }
        }
    }

    private float getTwigDropChance(ItemStack heldItemStack)
    {
        float chance = 0.0F;

        if (PrimalConfig.TWIGS.twigDropChanceVariesByTool)
        {
            if (heldItemStack.isEmpty())
            {
                chance = PrimalConfig.TWIGS.twigDropChanceWithBareHands;
            }
            else if (PrimalUtil.isAxe(heldItemStack.getItem()))
            {
                chance = PrimalConfig.TWIGS.twigDropChanceWithAxe;
            }
        }
        else
        {
            chance = 1.0F;
        }

        return chance;
    }

    private float getBranchDropChance(ItemStack heldItemStack)
    {
        float chance = 0.0F;

        if (PrimalConfig.BRANCHES_AND_SHAFTS.branchDropChanceVariesByTool)
        {
            if (heldItemStack.isEmpty())
            {
                chance = PrimalConfig.BRANCHES_AND_SHAFTS.branchDropChanceWithBareHands;
            }
            else if (PrimalUtil.isAxe(heldItemStack.getItem()))
            {
                chance = PrimalConfig.BRANCHES_AND_SHAFTS.branchDropChanceWithAxe;
            }
        }
        else
        {
            chance = 1.0F;
        }

        return chance;
    }

    // LOGS --> BARK

    @SubscribeEvent
    public void onPlayerInteract(PlayerInteractEvent.RightClickBlock event)
    {
        if (PrimalConfig.BARK_AND_STRIPPED_LOGS.allowBarkAndStrippedLogs)
        {
            if (!event.getWorld().isRemote && event.getResult() != Event.Result.DENY)
            {
                EntityPlayer player = event.getEntityPlayer();
                World world = event.getWorld();
                EnumHand hand = event.getHand();
                Item heldItem = player.getHeldItem(hand).getItem();
                BlockPos pos = event.getPos();
                IBlockState state = world.getBlockState(pos);
                Block block = state.getBlock();

                if (block instanceof BlockLog && PrimalUtil.isAxe(heldItem)) // && !(faceClicked == EnumFacing.DOWN || faceClicked == EnumFacing.UP))
                {
                    Item itemToDrop = PrimalUtil.getBark(state);
                    Block targetBlock = PrimalUtil.getStrippedLog(state);

                    // Replace the log with a stripped log that has the same axis
                    for (EnumFacing.Axis axis : EnumFacing.Axis.values())
                    {
                        // If the axis we're checking is the same as the axis of the log being broken
                        if (Objects.equals(axis.getName(), state.getValue(BlockLog.LOG_AXIS).getName()))
                        {
                            // Replace log with stripped log
                            IBlockState targetState = targetBlock.getDefaultState().withProperty(PrimalBlockLog.AXIS, axis);
                            world.setBlockState(event.getPos(), targetState);

                            // Destination of bark will be the block next to the face that was clicked
                            BlockPos barkDestination = pos;
                            EnumFacing faceClicked = event.getFace();

                            if (faceClicked != null)
                            {
                                barkDestination = barkDestination.offset(faceClicked);
                            }

                            // Spawn bark items
                            PrimalUtilSpawn.spawnEntityItem(world, barkDestination, itemToDrop, PrimalConfig.BARK_AND_STRIPPED_LOGS.barkAmount);
                            // heldItemStack.damageItem(1, player);

                            break;
                        }
                    }
                }
            }
        }
    }

    // BREAK ALL VINES BELOW CURRENT ONE

    @SubscribeEvent
    public void onBreakEvent(BlockEvent.BreakEvent event)
    {
        if (PrimalConfig.BASICS.cascadingVineBreakage)
        {
            World world = event.getWorld();
            IBlockState state = event.getState();
            Block block = state.getBlock();
            EntityPlayer player = event.getPlayer();
            Item heldItem = player.getHeldItemMainhand().getItem();

            if (block instanceof BlockVine && PrimalUtil.isAxe(heldItem) && player.isSneaking())
            {
                // Drop vine item(s) for the first block
                BlockPos pos = event.getPos();
                Item itemToDrop = Item.getItemFromBlock(Blocks.VINE);
                int count = BlockVine.getNumGrownFaces(state);

                PrimalUtilSpawn.spawnEntityItem(world, pos, itemToDrop, count);

                // Get block below the first one and initialize variables
                pos = pos.offset(EnumFacing.DOWN);
                state = world.getBlockState(pos);
                block = state.getBlock();
                ArrayList<BlockPos> vinesToDelete = new ArrayList<>();
                count = 0;

                // Check if blocks below are vines
                while (block instanceof BlockVine)
                {
                    // Block is a vine - add to list and increment counter
                    vinesToDelete.add(pos);
                    count += BlockVine.getNumGrownFaces(state);

                    // Get ready to check the next block below
                    pos = pos.offset(EnumFacing.DOWN);
                    state = world.getBlockState(pos);
                    block = state.getBlock();
                }

                // Delete all the vines in the list (must after adding to list, to avoid block update that deletes further vines before they can be checked
                for (BlockPos vinePosToDelete : vinesToDelete)
                {
                    if (world.getBlockState(vinePosToDelete).getBlock() instanceof BlockVine)
                    {
                        world.setBlockToAir(vinePosToDelete);
                    }
                }

                // Spawn vine items based on counter
                PrimalUtilSpawn.spawnEntityItem(world, pos, itemToDrop, count);
            }
        }
    }

    @SubscribeEvent
    public void onFuelBurnTime(FurnaceFuelBurnTimeEvent event)
    {
        if (Block.getBlockFromItem(event.getItemStack().getItem()) instanceof PrimalBlockLog)
        {
            event.setBurnTime(PrimalConfig.BARK_AND_STRIPPED_LOGS.strippedLogBurnTime);
        }
    }
}
