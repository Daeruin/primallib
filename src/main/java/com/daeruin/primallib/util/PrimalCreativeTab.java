package com.daeruin.primallib.util;

import com.daeruin.primallib.items.PrimalItemRegistry;
import com.google.common.collect.Ordering;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;

public class PrimalCreativeTab
{
    private static Comparator<ItemStack> creativeTabSorter;
    static CreativeTabs PRIMAL_TAB = new CreativeTabs("PrimalLib")
    {
        @Override
        public ItemStack createIcon()
        {
            return new ItemStack(PrimalItemRegistry.TWIG_GENERIC);
        }

        @Override
        public void displayAllRelevantItems(@Nonnull NonNullList<ItemStack> itemList)
        {
            super.displayAllRelevantItems(itemList); // Populates the list of items
            itemList.sort(creativeTabSorter); // Calls my Comparator to sort the list
        }
    };
    private static ArrayList<Item> order = new ArrayList<>(); // Holds the explicit order the items should be in

    // Called from preInit in main mod class - initializes the Comparator that's called from CreativeTabs.displayAllRelevantItems
    public static void addItemsToSortOrder(LinkedHashSet<Item> items)
    {
        for (Item item : items)
        {
            if (item != Items.AIR)
            {
                order.add(item);
            }
        }

        creativeTabSorter = Ordering.explicit(order).onResultOf(ItemStack::getItem);
    }

    public static void addBlocksToSortOrder(LinkedHashSet<Block> blocks)
    {
        for (Block block : blocks)
        {
            Item item = Item.getItemFromBlock(block);

            if (item != Items.AIR)
            {
                order.add(item);
            }
        }

        creativeTabSorter = Ordering.explicit(order).onResultOf(ItemStack::getItem);
    }
}
