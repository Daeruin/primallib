package com.daeruin.primallib.util;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class PrimalUtilSpawn
{
    public static void spawnEntityItem(World world, BlockPos pos, Item itemToDrop, int amount)
    {
        double spawnX = (double) pos.getX() + (double) (world.rand.nextFloat() * 0.5F) + 0.25D;
        double spawnY = (double) pos.getY() + (double) (world.rand.nextFloat() * 0.5F) + 0.25D;
        double spawnZ = (double) pos.getZ() + (double) (world.rand.nextFloat() * 0.5F) + 0.25D;

        ItemStack stack = new ItemStack(itemToDrop, amount, 0);
        EntityItem entityitem = new EntityItem(world, spawnX, spawnY, spawnZ, stack);

        entityitem.setDefaultPickupDelay();
        world.spawnEntity(entityitem);
    }

    public static void spawnFlame(World world, double xPos, double yPos, double zPos)
    {
        if (world.isRemote)
        {
            world.spawnParticle(EnumParticleTypes.FLAME, xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
        }
        else
        {
            ((WorldServer) world).spawnParticle(EnumParticleTypes.FLAME, xPos, yPos, zPos, 0, 0, 0, 0, 0, new int[0]);
        }
    }

    public static void spawnLittleSmoke(World world, double xPos, double yPos, double zPos)
    {
        if (world.isRemote)
        {
            world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
        }
        else
        {
            world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
        }
    }

    public static void spawnBigSmoke(World world, double xPos, double yPos, double zPos)
    {
        if (world.isRemote)
        {
            world.spawnParticle(EnumParticleTypes.SMOKE_LARGE, xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
        }
        else
        {
            ((WorldServer) world).spawnParticle(EnumParticleTypes.SMOKE_LARGE, xPos, yPos, zPos, 1, 0, 0, 0, 0, new int[0]);
        }
    }

    /**
     * Plays a sound for everyone at this entity's position, including the entity, at the default volume and pitch.
     *
     * @param entity The entity where the sound originates.
     * @param sound  The SoundEvent to be played.
     */
    public static void playSound(World world, Entity entity, SoundEvent sound)
    {
        if (entity instanceof EntityPlayerMP)
        {
            // Passing mull for the player ensures that the sound also plays for the EntityPlayerMP that's passed in
            world.playSound(null, entity.posX, entity.posY, entity.posZ, sound, entity.getSoundCategory(), 1.0F, 1.0F);
        }
        else
        {
            entity.playSound(sound, 1.0F, 1.0F);
        }
    }

    public static void playFireAudio(World world, double xPos, double yPos, double zPos)
    {
        if (world.isRemote)
        {
            world.playSound(xPos, yPos, zPos, SoundEvents.BLOCK_FIRE_AMBIENT, SoundCategory.BLOCKS, 0.1F, 1.0F, false);
        }
        else
        {
            world.playSound(xPos, yPos, zPos, SoundEvents.BLOCK_FIRE_AMBIENT, SoundCategory.BLOCKS, 0.1F, 1.0F, false);
        }
    }
}
