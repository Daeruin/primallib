package com.daeruin.primallib.blocks;

import com.daeruin.primallib.INeedsCustomModelReg;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.BlockRotatedPillar;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.ParametersAreNonnullByDefault;

public class PrimalBlockLog extends BlockRotatedPillar implements INeedsCustomModelReg
{
    protected PrimalBlockLog(String registryName)
    {
        super(Material.WOOD);
        PrimalUtilReg.initializeBlock(this, registryName);
        this.setHardness(2.0F);
        this.setSoundType(SoundType.WOOD);
    }

    @Override
    @ParametersAreNonnullByDefault
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        if (worldIn.isAreaLoaded(pos.add(-5, -5, -5), pos.add(5, 5, 5)))
        {
            for (BlockPos blockpos : BlockPos.getAllInBox(pos.add(-4, -4, -4), pos.add(4, 4, 4)))
            {
                IBlockState iblockstate = worldIn.getBlockState(blockpos);

                if (iblockstate.getBlock().isLeaves(iblockstate, worldIn, blockpos))
                {
                    iblockstate.getBlock().beginLeavesDecay(iblockstate, worldIn, blockpos);
                }
            }
        }
    }

    @Override
    public boolean canSustainLeaves(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos)
    {
        return false;
    }

    @Override
    public boolean isWood(net.minecraft.world.IBlockAccess world, BlockPos pos)
    {
        return true;
    }

    @Override
    public void registerCustomModel()
    {
        for (EnumFacing.Axis axis : EnumFacing.Axis.values()) // Need to register a model for every log variant. I don't know why.
        {
            Item item = Item.getItemFromBlock(this);
            PrimalUtilReg.registerModel(item, axis.ordinal(), PrimalUtilReg.getResourceLocation(item));
        }
    }
}